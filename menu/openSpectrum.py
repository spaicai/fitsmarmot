##########
# IMPORT #
##########
from PySide2 import QtWidgets
from PySide2.QtWidgets import*
import sys
from astropy.io import fits 
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import (FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure
from utils.spectrumManager import SpectrumManager

#########
# CLASS #
#########

class spectrumChooser(QWidget):
    """Open Spectrum Button Controller. Opens a file explorer, allowing you to open a fits/fit file.

    '''

    Attributes
    ----------
    parent : MyWindow
        The parent of the window, must contain an attribute ui which is the window where the spectrum will be displayed

    '''

    Methods
    -------
    initUI()
        Displays a file explorer and loads the selected file
    """

    def __init__(self, parent):
        """
        Parameters
        ----------
        parent : MyWindow
            The parent of the window, must contain an attribute ui which is the window where the spectrum will be displayed

        Raises:
        -------
        ValueError
            If the parent is None type
        """

        if parent == None:
            raise ValueError("parent can't be None type")

        self.parent = parent
        super().__init__()
        self.title = 'Choose a spectrum file'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

    def initUI(self):
        """Displays the file explorer and opens the selected file. Does nothing if no file is selected"""

        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self,"Choose a file", "","FITS Files (*.fit *.fits)", options=options)
        header = fits.open(fileName)[0].header
        if ('CDELT1' in header and 'CRVAL1' in header and 'CRPIX1' in header and 'NAXIS1' in header):         
            SpectrumManager.loadFitsFile(fileName, self.parent.ui)
        else:
            print("Error this file is not compatible with the spectrum generation")
