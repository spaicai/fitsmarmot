import os

print("="*24)
print("\nThis utility will walk you through creating a mod package.\n")

package_name = ""
while (package_name == "" or not package_name.replace("_","").isalnum()):
    package_name = input("Package name (only alphacharacter and _) : ")

module_name = ""
while (module_name == ""):
    module_name = input("Module name : ")

author = ""
while (author == ""):
    author = input("Author : ")


# Creation dossier

try:
    path = "mods/"+package_name
    os.mkdir(path)
except OSError:
    print ("Creation of the directory %s failed" % path)

# Creation __init__.py

f = open("mods/"+package_name+"/__init__.py","w+")
f.write("")
f.close()

# Creation manifest.json

manifest = "{\n\t\"Name\": \""+module_name+"\",\n\t\"Author\": \""+author+"\",\n\t\"Version\": \"0.1\",\n\t\"Description\": \"\",\n\t\"dependencies\": []\n}"

f2 = open("mods/"+package_name+"/manifest.json","w+")
f2.write(manifest)
f2.close()

# Creation package_name.py

code = "class "+package_name+":\n\tdef __init__(self):\n\t\tpass\n\n\t@staticmethod\n\tdef preload():\n\t\treturn "+package_name+"()\n\n\tdef load(self):\n\t\t#Here goes your code\n\t\tprint('Print from "+module_name+" module ')"
f3 = open("mods/"+package_name+"/"+package_name+".py", "w+")
f3.write(code)
f3.close()