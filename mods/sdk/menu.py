from .window import Window
from PySide2 import QtWidgets

class Menu:
    def __init__(self, name):
        self.name = name
        self.widget = QtWidgets.QMenu(self.name, Window.getInterface().ui.menubar)
        self.widget.setObjectName(self.name)
        self.actions = []
    
    def addAction(self, name, action):
        self.actions.append(QtWidgets.QAction(name, Window.getInterface().ui))
        self.actions[-1].setObjectName(name.replace(" ", "_"))
        self.actions[-1].triggered.connect(lambda checked: action())
        self.widget.addAction(self.actions[-1])

    def addSeparator(self):
        self.widget.addSeparator()