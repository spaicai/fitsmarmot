from .window import Window
from PySide2 import QtWidgets

class Grid:
    def __init__(self):
        self.widget = QtWidgets.QWidget()
        self.gridLayout = QtWidgets.QGridLayout(self.widget)

    def addGrid(self, grid, x, y, xw=1, yw=1):
        self.gridLayout.addWidget(grid.widget, x, y, xw, yw)

    def addText(self, text, x, y):
        label = QtWidgets.QLabel()
        label.setText(text)
        self.gridLayout.addWidget(label, x, y, 1, 1)

    def addFigure(self, fig, x, y):
        from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
        from matplotlib.backends.backend_qt5agg import (NavigationToolbar2QT as NavigationToolbar)
        canvas = FigureCanvas(fig)

        grid = Grid()
        grid.addQtWidget(canvas, x, y)
        grid.addQtWidget(NavigationToolbar(canvas, canvas), x+1, y)
        self.gridLayout.addWidget(grid.widget, x, y)

    def addButton(self, button, x, y):
        self.gridLayout.addWidget(button.widget, x, y)

    def addQtWidget(self, widget, x, y):
        self.gridLayout.addWidget(widget, x, y)

    def setBackgroundColor(self, hex):
        self.widget.setStyleSheet("background-color:"+hex+";")
