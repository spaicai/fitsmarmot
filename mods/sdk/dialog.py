from .window import Window
from .menu import Menu
from PySide2 import QtWidgets
from PySide2.QtUiTools import QUiLoader


class Dialog:
    # path = path to .ui file from view.py directory
    def __init__(self, name, path=None, width=600, height=600, createMenu=True):
        self.name = name
        menu = Menu(name)
        menu.addAction("Open "+name, self.showDialog)
        if createMenu:
            Window.addMenu(menu)

        if path != None:
            loader = QUiLoader()
            self.widget = loader.load(path)
        else:
            self.widget = QtWidgets.QDialog()
            self.widget.setWindowTitle(name)
        self.widget.resize(width, height)
        self.gridLayout = QtWidgets.QGridLayout(self.widget)

    def showDialog(self):
        self.widget.show()

    def setGrid(self, grid):
        self.gridLayout.addWidget(grid.widget, 0, 0)
