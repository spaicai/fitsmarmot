from .window import Window
from PySide2 import QtWidgets

class Button:
    def __init__(self, name, action):
        self.name = name
        self.widget = QtWidgets.QPushButton(self.name)
        self.widget.setObjectName(self.name)
        self.widget.clicked.connect(lambda checked: action())
