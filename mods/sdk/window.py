class Window:
    interface = None

    @staticmethod
    def setInterface(interface):
        Window.interface = interface

    @staticmethod
    def getInterface():
        return Window.interface

    @staticmethod
    def addMenu(menu):
        Window.getInterface().ui.menubar.addAction(menu.widget.menuAction())

    @staticmethod
    def addTab(tab, position="LEFT"):
        if position == "LEFT":
            Window.getInterface().ui.left.addTab(tab.widget, "")
            Window.getInterface().ui.left.setTabText(Window.getInterface().ui.left.indexOf(tab.widget), tab.name)
        else:
            Window.getInterface().ui.tabHeaderStats.addTab(tab.widget, "")
            Window.getInterface().ui.tabHeaderStats.setTabText(Window.getInterface().ui.tabHeaderStats.indexOf(tab.widget), tab.name)