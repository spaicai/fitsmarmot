from .window import Window
from PySide2 import QtWidgets

class Tab:
    def __init__(self, name):
        self.name = name
        self.widget = QtWidgets.QWidget()
        self.widget.setObjectName(name.replace(" ", "_"))

        self.gridLayout = QtWidgets.QGridLayout(self.widget)
        self.gridLayout.setObjectName("gridLayout_"+name.replace(" ", "_"))

    def setGrid(self, grid):
        while self.gridLayout.count():
            child = self.gridLayout.takeAt(0)
            if child.widget():
                child.widget().deleteLater()
        self.gridLayout.addWidget(grid.widget, 0, 0)
