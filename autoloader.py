import os
import time
import importlib
import json
from PySide2.QtUiTools import QUiLoader

from mods.sdk.window import Window
from utils.autoloaderHelper import AutoloaderHelper


class Autoloader:
    mods = []

    @staticmethod
    def installedPlugins():
        return os.listdir("./mods")

    @staticmethod
    def preloadPlugins(interface):
        Window.setInterface(interface)
        modsFile = open(".mods", "r")
        registeredMods = modsFile.read().splitlines()
        modsFile.close()
        modsList = []
        modsActiveList = []
        for l in registeredMods:
            mod, active = l.split("/")
            modsList.append(mod)
            modsActiveList.append(active)

        print("-"*24)
        print("Installed mods : ", registeredMods)
        print("-"*24)

        modsFile = open(".mods", "a+")
        plugins = Autoloader.installedPlugins()
        plugins.remove('sdk')
        plugins.remove('utils')
        for plugin in plugins:
            cls = getattr(importlib.import_module(
                "mods."+plugin+"."+plugin), plugin)

            if (plugin in modsList):
                index = modsList.index(plugin)
                if modsActiveList[index] == "1":
                    manifest = json.loads(open("mods/"+plugin+"/manifest.json").read())
                    print("Preloaded mod : "+manifest['Name']+" ("+manifest['Version']+") by "+manifest['Author'])
                    for i in manifest['dependencies']:
                        if i in modsList:
                            print('ok', i)
                        else:
                            print("[DÉPENDANCES] Problème de dépendance : le mod", plugin, "dépends de", i, "et la dépendance n'est pas satisfaite")
                            AutoloaderHelper.dependencyProblem(i, plugin)
                    Autoloader.mods.append(cls.preload())

            else:
                modsFile.write(plugin+"/1\n")
                modsList.append(plugin)
                modsActiveList.append("1")
                Autoloader.mods.append(cls.preload())

        Autoloader.modsList = modsList
        Autoloader.modsActiveList = modsActiveList
        modsFile.close()

    @staticmethod
    def loadMenu():
        AutoloaderHelper.loadMenu(Autoloader, Window)  # delegation

    @staticmethod
    def getLoadedMods():
        return Autoloader.mods

    @staticmethod
    def loadPlugins():
        print("\n"+"-"*24)
        for mod in Autoloader.mods:
            t1 = time.time()
            mod.load()
            print("Loaded mod : ", mod.__class__.__name__,
                  " in ", int((time.time()-t1)*1000), "ms")
        print("\n"+"-"*24, "\n")
