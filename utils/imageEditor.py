##########
# IMPORT #
##########

from PySide2 import QtWidgets, QtCore
import numpy as np

class ImageEditor:
    """Allows you to make changes in the brightness of the image
    
    '''

    Methods
    -------
    imageLoad(image_data, ax, ui, canvas)
        Displays the brightness management bar and links it to the photo
    brightnessChanged()
        Updates the brightness of the image
    """

    @staticmethod
    def imageLoad(image_data, ax, ui, canvas):
        """Displays the brightness management bar and links it to the photo

        Parameters
        ----------
        image_data : array
            Table containing the data of the fits image
        ax : axes.SubplotBase
            Canvas parameter axes
        ui : MyWindow
            Graphical interface of the software, containing the canvas parameter
        canvas : Figure
            Figure containing the image whose brightness will be adjustable
        """
        
        embedMPL = ui.findChildren(QtWidgets.QVBoxLayout, 'embedMPL')[0]
        image_data = image_data
        ImageEditor.image_data = image_data
        ImageEditor.ax = ax
        ImageEditor.im = ImageEditor.ax.imshow(image_data, cmap='gray')
        ImageEditor.canvas = canvas
               
        
        container = QtWidgets.QWidget()
        container.setMaximumHeight(80)
        horizontalLayout_14 = QtWidgets.QHBoxLayout()
        
        groupBox_3 = QtWidgets.QGroupBox()
        groupBox_3.setMaximumHeight(70)
        groupBox_3.setTitle("Brightness")
        ImageEditor.gp3 = groupBox_3
        gridLayout_14 = QtWidgets.QGridLayout(groupBox_3)
        
        sliderBrightness = QtWidgets.QSlider(groupBox_3)
        sliderBrightness.setOrientation(QtCore.Qt.Horizontal)
        sliderBrightness.setObjectName("sliderBrightness")
        max = np.amax(image_data)
        sliderBrightness.setMinimum(-1*max)
        sliderBrightness.setMaximum(55000-max)
        sliderBrightness.valueChanged.connect(ImageEditor.brightnessChanged)
        ImageEditor.sliderBrightness = sliderBrightness
        
        
        gridLayout_14.addWidget(sliderBrightness, 0, 0, 1, 1)
        horizontalLayout_14.addWidget(groupBox_3)

        container.setLayout(horizontalLayout_14)
        embedMPL.addWidget(container)
           
    def brightnessChanged():
        """Updates the brightness of the image"""

        ImageEditor.gp3.setTitle("Brightness ("+str(ImageEditor.sliderBrightness.value())+")")
        ImageEditor.im.set_data(ImageEditor.image_data+ImageEditor.sliderBrightness.value())
        ImageEditor.canvas.canvas.draw()

