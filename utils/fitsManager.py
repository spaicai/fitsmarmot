##########
# IMPORT #
##########

from PySide2 import QtWidgets
from astropy.io import fits
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import (NavigationToolbar2QT as NavigationToolbar)
from matplotlib.widgets import Cursor, RectangleSelector
from numpy import arange, amin, amax, average
from utils.imageEditor import ImageEditor
import tempfile
import re
import os
import shutil

class FitsManager:
    """Class that manages FITS/FIT files in order to be able to easily manipulate the data and display the photo.

    '''

    Attributes
    ----------
    cursor1 Cursor
        Crosshairs of the image
    cursor2 Cursor
        Crosshairs of the histogram
    rs1
        Selecting the image
    rs2
        Selecting the histogram
    headerEntryCount int
        counter of the number of fields of the header of the open file
    openedFile str
        path of the open file

    '''

    Methods
    -------
    loadFitsFile(filePath, ui)
        Retrieves the data present in the header and displays the photograph in the software
    onclick(event):
        Updates the cursor position on the image when moving
    line_select_callback(eclick, erelease):
        Allows you to retrieve the data contained in the selection frame on the image. These data are used to compile statistics.
    toggle_selector(event):
        Show event
    onclick2(event)
        Updates the cursor position on the histogram when moving
    line_select_callback2(eclick, erelease)
        Updates the cursor position on the histogram when moving
    toggle_selector2(event)
        Show event
    changeMode(ui)
        Enables or disables the selection mode. This mode displays a blue square on the photo, allowing you to select an area where statistics will be made.
        Statistics are done on the whole image otherwise.
    editFitsFileHeader(datalist,ui)
        Saves the changes made to the header in the open fits file.
    """

    cursor1 = None
    cursor2 = None
    rs1 = None
    rs2 = None
    headerEntryCount = 0;
    openedFile = "";
    

    @staticmethod
    def loadFitsFile(filePath, ui):
        """Retrieves the data present in the header and displays the photograph in the software

        Parameters
        ----------
        filePath : str
            contains the path to the file to be loaded
        ui : MyWindow
            the software interface where the data and the photograph will be displayed
        """
        FitsManager.openedFile = filePath;

        embedMPL = ui.findChildren(QtWidgets.QVBoxLayout, 'embedMPL')[0]
        ui.findChildren(QtWidgets.QTabWidget, 'left')[0].setTabText(1, "Image")

        for i in reversed(range(embedMPL.count())):
            embedMPL.itemAt(i).widget().setParent(None)

        image_data = fits.getdata(filePath)
        fig = Figure(figsize=(4, 4), dpi=100)
        ax = fig.add_subplot(111)
        ax.imshow(image_data, cmap='gray')
        ax.set_xlabel("Pixels")
        ax.set_ylabel("Pixels")

        
        canvas = FigureCanvas(fig)
        embedMPL.addWidget(canvas)
        embedMPL.addWidget(NavigationToolbar(canvas, canvas))
        cursor = Cursor(ax, useblit=False, color='white', linewidth=1)

        button = ui.findChildren(QtWidgets.QPushButton, 'pushButton')[0]
        button.setEnabled(True)

        ImageEditor.imageLoad(image_data, ax, ui, fig)


        def onclick(event):
            """Updates the cursor position on the image when moving

            Parameters
            ----------
            event : 
                Allows the cursor to move
            """
            cursor.onmove(event)

        canvas.mpl_connect('button_press_event', onclick)
        FitsManager.cursor1 = cursor
        
        def line_select_callback(eclick, erelease):
            """Allows you to retrieve the data contained in the selection frame on the image. These data are used to compile statistics.

            Parameters
            ----------
            eclick : 
                the coordinates where the click is made
            erelease : 
                the coordinates where the mouse is release
            """
            x1, y1 = eclick.xdata, eclick.ydata #Get the x and y click
            x2, y2 = erelease.xdata, erelease.ydata #Get the x and y click


            hdul = fits.open(filePath) #Retrieving the file path
            data = hdul[0].data #Recovery of file data

            # Retrieving widgets of statistics
            minPixSel = ui.findChildren(QtWidgets.QLabel, 'minPixSel')[0] 
            avgPixSel = ui.findChildren(QtWidgets.QLabel, 'avgPixSel')[0] 
            maxPixSel = ui.findChildren(QtWidgets.QLabel, 'maxPixSel')[0]

            #Set the minimum average maximum of the selected pixel list 
            minPixSel.setText(str(amin(data[int(x1):int(x2), int(y1):int(y2)]))) 
            avgPixSel.setText(str(average(data[int(x1):int(x2), int(y1):int(y2)]))) 
            maxPixSel.setText(str(amax(data[int(x1):int(x2), int(y1):int(y2)])))
            
        def toggle_selector(event):
            """Print event
            
            Parameters
            ----------
            event : 
                event on the selector one
            """
            print('event')

        toggle_selector.RS = RectangleSelector(ax, line_select_callback, useblit=True, minspanx=5, minspany=5, spancoords='pixels', interactive=True, rectprops = dict(facecolor='blue', edgecolor = 'white', alpha=0.2, fill=True))
        canvas.mpl_connect('key_press_event', toggle_selector)
        FitsManager.rs1 = toggle_selector.RS

        # Retrieving the header and inserting it into the table
        table = ui.findChildren(QtWidgets.QTableWidget, 'tableWidget')[0]

        table.setRowCount(0)

        header = fits.getheader(filePath)
        i = 0
        for line in header:
            table.insertRow(table.rowCount())
            table.setItem(table.rowCount() - 1, 0, QtWidgets.QTableWidgetItem(line))
            table.setItem(table.rowCount() - 1, 1, QtWidgets.QTableWidgetItem(str(header[i])))
            i = i + 1
        FitsManager.headerEntryCount = i; # Save the number of entries in the header

        # Retrieving widgets of statistics
        minPixVal = ui.findChildren(QtWidgets.QLabel, 'minPixVal')[0]
        avgPixVal = ui.findChildren(QtWidgets.QLabel, 'avgPixVal')[0]
        maxPixVal = ui.findChildren(QtWidgets.QLabel, 'maxPixVal')[0]

        minPixSel = ui.findChildren(QtWidgets.QLabel, 'minPixSel')[0]
        avgPixSel = ui.findChildren(QtWidgets.QLabel, 'avgPixSel')[0]
        maxPixSel = ui.findChildren(QtWidgets.QLabel, 'maxPixSel')[0] 

        hdul = fits.open(filePath) #Retrieving the file path
        data = hdul[0].data #Recovery of file data

        #Set value minimum maximum and average of the pixel list
        minPixVal.setText(str(amin(data))) 
        avgPixVal.setText(str(average(data))) 
        maxPixVal.setText(str(amax(data))) 

        #Set the minimum maximum and average of the selected pixel list 
        minPixSel.setText(str(amin(data))) 
        avgPixSel.setText(str(average(data))) 
        maxPixSel.setText(str(amax(data))) 
        
        # Display of simple data, by putting / if there is no data in the header
        spDate = '/'
        if ('DATE' in header):
            spDate = header['DATE']
        if ('DATE-OBS' in header):
            spDate = header['DATE-OBS']
        ui.findChildren(QtWidgets.QLabel, 'spDate')[0].setText(str(spDate))
        
        spSiteLat = '/'
        if ('SITELAT' in header):
            spSiteLat = header['SITELAT']
        ui.findChildren(QtWidgets.QLabel, 'spSiteLat')[0].setText(str(spSiteLat))
        
        spSiteLong = '/'
        if ('SITELONG' in header):
            spSiteLong = header['SITELONG']
        ui.findChildren(QtWidgets.QLabel, 'spSiteLong')[0].setText(str(spSiteLong))
        
        spTelescop = '/'
        if ('TELESCOP' in header):
            spTelescop = header['TELESCOP']
        ui.findChildren(QtWidgets.QLabel, 'spTelescop')[0].setText(str(spTelescop))
        
        spInstrument = '/'
        if ('INSTRUME' in header):
            spInstrument = header['INSTRUME']
        ui.findChildren(QtWidgets.QLabel, 'spInstrument')[0].setText(str(spInstrument))
        
        spFilter = '/'
        if ('FILTER' in header):
            spFilter = header['FILTER']
        ui.findChildren(QtWidgets.QLabel, 'spFilter')[0].setText(str(spFilter))
        
        spExposure = '/'
        if ('EXPOSURE' in header):
            spExposure = header['EXPOSURE']
        ui.findChildren(QtWidgets.QLabel, 'spExposure')[0].setText(str(spExposure))
        
        spBitPix = '/'
        if ('BITPIX' in header):
            spBitPix = header['BITPIX']
        ui.findChildren(QtWidgets.QLabel, 'spBitPix')[0].setText(str(spBitPix))

        menuOpened_file = ui.findChildren(QtWidgets.QMenu, 'menuOpened_file')[0]
        menuOpened_file.setTitle("Opened File : "+re.search(r"(/|\\)(\b[^\\/]*\b)(\.fits|\.fit)",FitsManager.openedFile).group(2))

        embedMPLgraph = ui.findChildren(QtWidgets.QVBoxLayout, 'embedMPLgraph')[0]
        for i in reversed(range(embedMPLgraph.count())):
            embedMPLgraph.itemAt(i).widget().setParent(None)

        fig = Figure(figsize=(4, 4), dpi=100)
        t = arange(0, 3, .01)
        ax2 = fig.add_subplot(111)
        ax2.hist(image_data.flatten(), 400)
        ax2.set_xlabel("Pixel value")
        ax2.set_ylabel("Number of pixels at the value")
        canvasG = FigureCanvas(fig)
        embedMPLgraph.addWidget(canvasG)
        embedMPLgraph.addWidget(NavigationToolbar(canvasG, canvasG))
        cursor2 = Cursor(ax2, useblit=False, color='black', linewidth=1)


        def onclick2(event):
            """Updates the cursor position on the histogram when moving
            
            Parameters
            ----------
            event :
                Allows the cursor to move
            """
            cursor2.onmove(event)
        canvasG.mpl_connect('button_press_event', onclick2)
        FitsManager.cursor2 = cursor2
        
        def line_select_callback2(eclick, erelease):
            """Allows you to retrieve the data contained in the selection frame on the histogram. These data are used to compile statistics.

            Parameters
            ----------
            eclick : 
                the coordinates where the click is made
            erelease :
                the coordinates where the mouse is release
            """
            x1, y1 = eclick.xdata, eclick.ydata
            x2, y2 = erelease.xdata, erelease.ydata

           
            
        def toggle_selector2(event):
            """Print event
            
            Parameters
            ----------
            event : 
                event on the selector two
            """
            print('event')

        toggle_selector.RS2 = RectangleSelector(ax2, line_select_callback2, useblit=True, minspanx=5, minspany=5, spancoords='pixels', interactive=True, rectprops = dict(facecolor='black', edgecolor = 'black', alpha=0.4, fill=True))
        canvasG.mpl_connect('key_press_event', toggle_selector2)
        FitsManager.rs2 = toggle_selector.RS2
        FitsManager.rs1.active = False
        FitsManager.cursor1.active = True
        FitsManager.rs2.active = False
        
        tabWidget = ui.findChildren(QtWidgets.QTabWidget, 'left')[0]
        tabWidget.setCurrentWidget(tabWidget.findChildren(QtWidgets.QWidget, 'tab')[0])

    def changeMode(ui):
        """Enables or disables the selection mode. This mode displays a blue square on the photo, allowing you to select an area where statistics will be made.
        Statistics are done on the whole image otherwise.
        
        Parameters
        ----------
        ui : MyWindow
            The window containing the image and the button activating the selection.
        """

        if (FitsManager.cursor1 == None):
            ui.findChildren(QtWidgets.QPushButton, 'selectionBtn')[0].setChecked(False)
            pass
        elif (FitsManager.cursor1.active == True): # si le CURSOR est ON
            FitsManager.cursor1.active = False
            FitsManager.cursor2.active = False
            
            FitsManager.rs1.active = True
            FitsManager.rs2.active = True
            
        else:
            FitsManager.rs1.active = False
            FitsManager.rs2.active = False
            FitsManager.cursor1.active = True
            FitsManager.cursor2.active = True

    def editFitsFileHeader(datalist,ui):
        """Saves the changes made to the header of the open fits file.
        
        Parameters
        ----------
        datalist : QTableWidget
            table containing all the header data.
        ui : MyWindow
            Window displaying the photograph and header information. Will be updated after the changes made in the header.
        """
        fileName = re.search(r"(/|\\)(\b[^\\/]*\b)(\.fits|\.fit)", FitsManager.openedFile).group(2)  # Retrieve the name of the loaded file
        fileExt = re.search(r"(/|\\)(\b[^\\/]*\b)(\.fits|\.fit)", FitsManager.openedFile).group(3)  # Retrieve the extension of the loaded file
        temp = tempfile.NamedTemporaryFile(mode="w", prefix=fileName + str("-"), suffix=fileExt, delete=False)  # Create a temporary file to save to store modified file, modifier to keep file even if closed, name with the original name of the file, a random string, and it's extension
        temp.close() # Close the temporary file

        file = fits.open(FitsManager.openedFile, mode='update')
        for i in range(0, FitsManager.headerEntryCount):
            value = datalist.item(i, 1).text()  # Retrieving the value of the list
            file[0].header[i] = type(file[0].header[i])(value)  # Assigning the list value to the header
        file[0].writeto(temp.name, overwrite=True)  # Writing the modified FITS file
        file.close()

        os.remove(FitsManager.openedFile) # Remove the opened fits file you edited
        shutil.move(temp.name,FitsManager.openedFile) # Move the temporary file to the path where the was the edited file
        FitsManager.loadFitsFile(FitsManager.openedFile,ui) # Load the new file which contain edited datas