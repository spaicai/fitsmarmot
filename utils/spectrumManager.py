##########
# IMPORT #
##########

from PySide2 import QtWidgets
from astropy.io import fits
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import (NavigationToolbar2QT as NavigationToolbar)
from matplotlib.widgets import Cursor, RectangleSelector
from numpy import arange, amin, amax, average
from specutils import Spectrum1D
import astropy.units as u
from utils.imageEditor import ImageEditor
import tempfile
import re
import os
import shutil

class SpectrumManager:
    """Class managing FITS/FIT files containing only a spectrum

    '''

    Attributes
    ----------
    headerEntryCount int
        counter of the number of fields of the header of the open file
    openedFile str
        path of the open file

    '''

    Methods
    -------
    loadFitsFile(filePath, ui)
        Retrieves the data present in the header and displays the spectrum in the software
    onclick(event)
        Updates the cursor position on the image when moving
    changeMode(ui)
        Enables or disables the selection mode. This mode displays a blue square on the graph representing the spectrum, allowing you to select an area where statistics will be made.
        Statistics are done on the whole graph otherwise.
    editFitsFileHeader(datalist,ui)
        Saves the changes made to the header in the open fits file.
    """

    headerEntryCount = 0;
    openedFile = "";

    @staticmethod
    def loadFitsFile(filePath, ui):
        """Retrieves the data present in the header and displays the spectrum in the software
        
        Parameters
        ----------
        filePath : str
            path to the fits file
        ui : MyWindow
            Software window where the spectrum will be displayed
        """

        SpectrumManager.openedFile = filePath;

        embedMPL = ui.findChildren(QtWidgets.QVBoxLayout, 'embedMPL')[0]
        ui.findChildren(QtWidgets.QTabWidget, 'left')[0].setTabText(1, "Spectre")

        for i in reversed(range(embedMPL.count())):
            embedMPL.itemAt(i).widget().setParent(None)
            
        embedMPLgraph = ui.findChildren(QtWidgets.QVBoxLayout, 'embedMPLgraph')[0]
        for i in reversed(range(embedMPLgraph.count())):
            embedMPLgraph.itemAt(i).widget().setParent(None)

        image_data = fits.getdata(filePath)
        fig = Figure(figsize=(4, 4), dpi=100)
        ax = fig.add_subplot(111)
        hdul = fits.open(filePath)
        data = hdul[0].data # données spectre
        header = hdul[0].header # header

        maxPix = header['NAXIS1'] # pixel maximal
        refPix = header['CRPIX1']-1 # pixel de reference

        # Conversion pixels en longueurs d'onde 
        cdelt = header['CDELT1']
        lambda1 = header['CRVAL1'] - cdelt * refPix
        lambda2 = header['CRVAL1'] + cdelt * (maxPix - refPix)

        l = arange(lambda1, lambda2, cdelt) * u.AA # lambda
        i=data * u.Jy # flux

        spec1d = Spectrum1D(spectral_axis=l, flux=i)
        ax.plot(spec1d.spectral_axis, spec1d.flux)
        ax.set_xlabel("Longueur d'onde (A)")
        ax.set_ylabel("Intensité relative")
        canvas = FigureCanvas(fig)
        embedMPL.addWidget(canvas)
        embedMPL.addWidget(NavigationToolbar(canvas, canvas))
        cursor = Cursor(ax, useblit=False, color='black', linewidth=1)
        
        def onclick(event):
            """ Updates the cursor position on the image when moving
            
            Parameters
            ----------
            event : 
                Allows the cursor to move
            """
            cursor.onmove(event)

        canvas.mpl_connect('button_press_event', onclick)

        button = ui.findChildren(QtWidgets.QPushButton, 'pushButton')[0]
        button.setEnabled(True)

        # Retrieving the header and inserting it into the table
        table = ui.findChildren(QtWidgets.QTableWidget, 'tableWidget')[0]

        #table.clear()
        table.setRowCount(0)

        header = fits.getheader(filePath)
        i = 0
        for line in header:
            table.insertRow(table.rowCount())
            table.setItem(table.rowCount() - 1, 0, QtWidgets.QTableWidgetItem(line))
            table.setItem(table.rowCount() - 1, 1, QtWidgets.QTableWidgetItem(str(header[i])))
            i = i + 1
        SpectrumManager.headerEntryCount = i;

        menuOpened_file = ui.findChildren(QtWidgets.QMenu, 'menuOpened_file')[0]
        menuOpened_file.setTitle("Opened File : "+re.search(r"(/|\\)(\b[^\\/]*\b)(\.fits|\.fit)",SpectrumManager.openedFile).group(2))
        
        tabWidget = ui.findChildren(QtWidgets.QTabWidget, 'left')[0]
        tabWidget.setCurrentWidget(tabWidget.findChildren(QtWidgets.QWidget, 'tab')[0])
        
        # Display of simple data, by putting / if there is no data in the header
        spDate = '/'
        if ('DATE' in header):
            spDate = header['DATE']
        if ('DATE-OBS' in header):
            spDate = header['DATE-OBS']
        ui.findChildren(QtWidgets.QLabel, 'spDate')[0].setText(str(spDate))
        
        spSiteLat = '/'
        if ('SITELAT' in header):
            spSiteLat = header['SITELAT']
        ui.findChildren(QtWidgets.QLabel, 'spSiteLat')[0].setText(str(spSiteLat))
        
        spSiteLong = '/'
        if ('SITELONG' in header):
            spSiteLong = header['SITELONG']
        ui.findChildren(QtWidgets.QLabel, 'spSiteLong')[0].setText(str(spSiteLong))
        
        spTelescop = '/'
        if ('TELESCOP' in header):
            spTelescop = header['TELESCOP']
        ui.findChildren(QtWidgets.QLabel, 'spTelescop')[0].setText(str(spTelescop))
        
        spInstrument = '/'
        if ('INSTRUME' in header):
            spInstrument = header['INSTRUME']
        ui.findChildren(QtWidgets.QLabel, 'spInstrument')[0].setText(str(spInstrument))
        
        spFilter = '/'
        if ('FILTER' in header):
            spFilter = header['FILTER']
        ui.findChildren(QtWidgets.QLabel, 'spFilter')[0].setText(str(spFilter))
        
        spExposure = '/'
        if ('EXPOSURE' in header):
            spExposure = header['EXPOSURE']
        ui.findChildren(QtWidgets.QLabel, 'spExposure')[0].setText(str(spExposure))
        
        spBitPix = '/'
        if ('BITPIX' in header):
            spBitPix = header['BITPIX']
        ui.findChildren(QtWidgets.QLabel, 'spBitPix')[0].setText(str(spBitPix))

    def changeMode(ui):
        """Enables or disables the selection mode. This mode displays a blue square on the graph representing the spectrum, allowing you to select an area where statistics will be made.
        Statistics are done on the whole graph otherwise.
        
        Parameters
        ----------
        ui : MyWindow
            La fenetre contenant l’image et le bouton activant la sélection.
            The window containing the image and the button activating the selection.
        """

        ui.findChildren(QtWidgets.QPushButton, 'selectionBtn')[0].setChecked(False)
        pass

    def editFitsFileHeader(datalist,ui):
        """Saves the changes made to the header in the open fits file.
        
        Parameters
        ----------
        datalist : QTableWidget
            table containing all the header data.
        ui : MyWindow
            Window displaying the spectrum and header information. Will be updated after the changes made in the header.
        """

        fileName = re.search(r"(/|\\)(\b[^\\/]*\b)(\.fits|\.fit)",SpectrumManager.openedFile).group(2)
        fileExt = re.search(r"(/|\\)(\b[^\\/]*\b)(\.fits|\.fit)", SpectrumManager.openedFile).group(3)
        temp = tempfile.NamedTemporaryFile(mode="w", prefix=fileName+str("-"),suffix=fileExt,delete=False)
        temp.close()

        file = fits.open(SpectrumManager.openedFile, mode='update')
        for i in range(0, SpectrumManager.headerEntryCount):
            value = datalist.item(i, 1).text()  # Retrieving the value of the list
            file[0].header[i] = type(file[0].header[i])(value)  # Assigning the list value to the header
        file[0].writeto(temp.name, overwrite=True)  # Writing the modified FITS file
        file.close()

        os.remove(SpectrumManager.openedFile)
        shutil.move(temp.name,SpectrumManager.openedFile)
        SpectrumManager.loadFitsFile(SpectrumManager.openedFile,ui)