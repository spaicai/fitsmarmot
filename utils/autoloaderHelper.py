import urllib
import io
import json
from zipfile import ZipFile

from mods.sdk.dialog import Dialog
from mods.sdk.grid import Grid
from mods.sdk.button import Button

from PySide2 import QtWidgets
from PySide2.QtUiTools import QUiLoader


class AutoloaderHelper:
    @staticmethod
    def clearLayout(layout):
        while layout.count():
            child = layout.takeAt(0)
            if child.widget():
                child.widget().deleteLater()

    @staticmethod
    def loadMenu(Autoloader, Window):
        loader = QUiLoader()
        ModsMenu = loader.load('ModsMenu.ui')
        ModsMenu.resize(600, 600)

        ModsMenu.push_reloadMods.clicked.connect(lambda checked: reloadMods())

        def reloadMods():
            AutoloaderHelper.clearLayout(ModsMenu.verticalLayout)
            oneMods = []
            i = 0
            for i in range(0, len(Autoloader.modsList)):
                oneMods.append(AutoloaderHelper.createMod(
                    i, ModsMenu.mods_list, ModsMenu.verticalLayout, Autoloader))
            ModsMenu.verticalLayout.addItem(QtWidgets.QSpacerItem(
                20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding))

            f = open(".mods", "w")
            for i in range(0, len(Autoloader.modsList)):
                f.write(Autoloader.modsList[i]+"/" +
                        Autoloader.modsActiveList[i]+"\n")
            f.close()

        ModsMenu.push_availableMods.clicked.connect(
            lambda checked: reloadAvailableMods())

        def reloadAvailableMods():
            print("START: call to api")
            api = urllib.request.urlopen(
                'https://stellaria.space/data/available-mods.php').read().decode('utf-8')
            print("END: call to api")

            availableMods = json.loads(api)['mods']

            AutoloaderHelper.clearLayout(ModsMenu.verticalLayout_2)
            Autoloader.availableMods = []

            for i in range(0, len(availableMods)):
                Autoloader.availableMods.append(availableMods[i])
                AutoloaderHelper.createAvailableMod(i, ModsMenu.available_mods_list,
                                                    ModsMenu.verticalLayout_2, Autoloader)
            ModsMenu.verticalLayout_2.addItem(QtWidgets.QSpacerItem(
                20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding))

        Window.getInterface().ui.actionOpen_mods.triggered.connect(
            lambda checked: openModsMenu())
        reloadMods()

        def openModsMenu():
            ModsMenu.show()

    @staticmethod
    def createMod(index, modsList, verticalLayout, Autoloader):
        one_mod = QtWidgets.QWidget(modsList)
        one_mod.setObjectName("one_mod")
        horizontalLayout_2 = QtWidgets.QHBoxLayout(one_mod)
        horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        mod_label = QtWidgets.QLabel(one_mod)
        horizontalLayout_2.addWidget(mod_label)
        widget = QtWidgets.QWidget(one_mod)
        horizontalLayout_3 = QtWidgets.QHBoxLayout(widget)
        push_mod_enable = QtWidgets.QPushButton(widget)
        horizontalLayout_3.addWidget(push_mod_enable)
        horizontalLayout_2.addWidget(widget)
        verticalLayout.addWidget(one_mod)

        activeDict = {"0": "DEACTIVATED", "1": "ACTIVATED"}
        text = Autoloader.modsList[index] + \
            " (" + activeDict[Autoloader.modsActiveList[index]] + ")"
        mod_label.setText(text)
        if Autoloader.modsActiveList[index] == "1":
            push_mod_enable.setText("Disable")
        else:
            push_mod_enable.setText("Enable")
        push_mod_enable.clicked.connect(lambda checked: enableMod(index))

        def enableMod(index):
            if Autoloader.modsActiveList[index] == "1":
                Autoloader.modsActiveList[index] = "0"
                push_mod_enable.setText("Enable")
            else:
                Autoloader.modsActiveList[index] = "1"
                push_mod_enable.setText("Disable")

        return one_mod

    @staticmethod
    def createAvailableMod(index, modsList, verticalLayout, Autoloader):
        one_mod = QtWidgets.QWidget(modsList)
        one_mod.setObjectName("one_available_mod")
        horizontalLayout_2 = QtWidgets.QHBoxLayout(one_mod)
        horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        mod_label = QtWidgets.QLabel(one_mod)
        mod_label.setText(Autoloader.availableMods[index])
        horizontalLayout_2.addWidget(mod_label)
        widget = QtWidgets.QWidget(one_mod)
        horizontalLayout_3 = QtWidgets.QHBoxLayout(widget)
        push_mod_download = QtWidgets.QPushButton(widget)
        push_mod_download.setText("Download")
        horizontalLayout_3.addWidget(push_mod_download)
        horizontalLayout_2.addWidget(widget)
        verticalLayout.addWidget(one_mod)

        push_mod_download.clicked.connect(lambda checked: downloadMod(index))

        def downloadMod(index):
            modBuffer = urllib.request.urlopen(
                "https://stellaria.space/data/mods/download/" +
                Autoloader.availableMods[index]+".zip").read()
            z = ZipFile(io.BytesIO(modBuffer))
            z.extractall("mods")

            recipe = urllib.request.urlopen(
                "https://stellaria.space/data/mods/download/" +
                Autoloader.availableMods[index]+"-recipe.html").read().decode('utf-8')

            dialog = Dialog("recipe", createMenu=False)
            grid = Grid()
            dialog.setGrid(grid)
            recipeWidget = QtWidgets.QTextBrowser(
                grid.widget)
            recipeWidget.setHtml(
                "<!DOCTYPE HTML><html><head><meta name=\"qrichtext\" content=\"1\" /></head><body>"+recipe+"</body></html>")
            grid.addQtWidget(recipeWidget, 0, 0)
            dialog.showDialog()
            print(dialog.widget)

            print("[MOD]", Autoloader.availableMods[index],
                  "installed. Please restart the application")
        return one_mod

    @staticmethod
    def dependencyProblem(modName, dependantName):
        api = urllib.request.urlopen(
            'https://stellaria.space/data/available-mods.php').read().decode('utf-8')
        availableMods = json.loads(api)['mods']
        if modName in availableMods:
            dialog = Dialog("", width=600, height=200)
            grid = Grid()
            dialog.setGrid(grid)
            grid.addText("[DÉPENDANCES] Problème de dépendance : le mod "+dependantName+" dépends de "+modName +
                         " et la dépendance n'est pas satisfaite.\nCependant, la dépendance a été trouvée dans le dépot en ligne.\nVoulez vous la télécharger ?", 0, 0)
            AutoloaderHelper.lastDependency = modName

            def download():
                modBuffer = urllib.request.urlopen(
                    "https://stellaria.space/data/mods/download/" +
                    AutoloaderHelper.lastDependency+".zip").read()
                z = ZipFile(io.BytesIO(modBuffer))
                z.extractall("mods")
                print("[MOD]", AutoloaderHelper.lastDependency,
                      "installed. Please restart the application")

            button = Button("Download", download)
            grid.addButton(button, 1, 0)
            dialog.widget.show()
