#########
# IMPORT #
#########

from PySide2 import QtGui, QtWidgets, QtCore
from PySide2.QtUiTools import QUiLoader
from astropy.io import fits
import sys, re

from menu.quit import quit
from menu.openFile import fileChooser
from menu.openSpectrum import spectrumChooser
from menu.closeFile import closePicture
from utils.fitsManager import FitsManager
from utils.spectrumManager import SpectrumManager

from autoloader import Autoloader

# ------------------ MyWindow ------------------
class MyWindow(QtWidgets.QMainWindow):
    """Main display class
    
    Attributes
    ----------
    ui : PySide2.QtWidgets.QMainWindow
        main display
    
    Methods
    -------
    load()
        loads the interface. Must be followed by .show() method to display the window
    dragEnterEvent(event)
        Allows to retrieve the information of the file that is put on the window
    dropEvent(event)
        Allows you to manage the actions that will be performed when the file is dropped on the window 
    """

    def load(self):
        """loads the interface. Must be followed by .show() method to display the window"""
        self.setWindowTitle("StellarAI")
        loader = QUiLoader()
        self.ui = loader.load('interface.ui')
        print()
        print("-"*24)
        print("Starting StellarIA app", "\n")
        self.setAcceptDrops(True)
        self.ui.setAcceptDrops(True)

        fc = fileChooser(parent=self)
        sc = spectrumChooser(parent=self)
        cp = closePicture(parent=self)
        self.ui.actionOpen_file.triggered.connect(lambda checked: fc.initUI())
        self.ui.actionOpen_Spectrum.triggered.connect(lambda checked: sc.initUI())
        self.ui.actionClose_Picture.triggered.connect(lambda checked: cp.close())
        self.ui.actionQuit.triggered.connect(quit)

        #Retrieve the button to save modifications to fits file's header, add the method to handle saving
        list = self.ui.findChildren(QtWidgets.QTableWidget, 'tableWidget')[0]
        saveButton = self.ui.findChildren(QtWidgets.QPushButton, 'pushButton')[0]
        saveButton.clicked.connect(lambda: FitsManager.editFitsFileHeader(list,self))

        selectBtn = self.ui.findChildren(QtWidgets.QPushButton, 'selectionBtn')[0]
        selectBtn.clicked.connect(lambda checked: FitsManager.changeMode(self.ui))

        self.setCentralWidget(self.ui)
        
        Autoloader.preloadPlugins(self)
        Autoloader.loadMenu()
        Autoloader.loadPlugins()

    def dragEnterEvent(self, event):
        """Allows to retrieve the information of the file that is put on the window

        Parameters
        ----------
        event : PySide2.QtGui.QDragEnterEvent
            Event that is triggered when the file is on the window. Contains information such as the path to the file.
        """

        mime = event.mimeData()
        urls = mime.urls()[0]
        if len(mime.urls()) == 1 and re.match("PySide2.QtCore.QUrl\('[\w\W]*.(fits|fit)\'\)", str(urls)) != None :
            event.acceptProposedAction()

    def dropEvent(self, event):
        """Allows you to manage the actions that will be performed when the file is dropped on the window 
    
        Parameters
        ----------
        event : PySide2.QtGui.QDragEnterEvent
            Event that is triggered when the file is on the window. Contains information such as the path to the file.
        """

        for url in event.mimeData().urls():
            file_name = url.toLocalFile()
            header = fits.open(file_name)[0].header
            if ('CDELT1' in header and 'CRVAL1' in header and 'CRPIX1' in header and 'NAXIS1' in header):
                SpectrumManager.loadFitsFile(file_name, self)
            else:
                FitsManager.loadFitsFile(file_name, self)
        return super().dropEvent(event)

app = QtWidgets.QApplication(sys.argv)
window = MyWindow()
app.setWindowIcon(QtGui.QIcon('logo.png'))
window.load()
window.resize(1280, 720)
window.show()
app.exec_()
