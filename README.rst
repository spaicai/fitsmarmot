FitsMarmot
Welcome to FitsMarmot's deposit!
--------------------------------

Fits Viewer is a prototype viewer for spectroscopy and fits images files.
This tools was initialy develop for the SPAICAI Project (Spectrocopic Calibration with Deep Learning)
More information here : https://sccn.stellartrip.net/

|python| |dependencies| |licence|

Getting started
---------------
#todo

Features
--------

- Show FITS Spectrum
- Show FITS image files

Installation
------------

Environments recommendation

Python requirements (Python >= 3.7)

Required PIP packages : https://pip.pypa.io/en/stable/installing/ 
(use requirements.txt for a quick install) 

PySide2 : https://pypi.org/project/PySide2/
AstroPy : https://www.astropy.org/
SpecUtils : https://specutils.readthedocs.io/en/stable/


Contribute
----------

Please read CONTRIBUTING.rst for details on our code of conduct, and the process for submitting pull requests to us.

Support
-------

If you have issues, please let us know.
You can ask questions on Stellartrip page : https://stellartrip.net/contact/


.. |python| image:: https://img.shields.io/badge/Python-3.7%203.8-green
    :alt: Python
    :scale: 100%
    :target: https://www.python.org/

.. |dependencies| image:: https://img.shields.io/badge/dependencies-astropy-orange
    :alt: Dependencies
    :scale: 100%
    :target: https://www.astropy.org/

.. |licence| image:: https://img.shields.io/badge/licence-GPL%203.0-orange
    :alt: Licence gpl-v3.0
    :scale: 100%
    :target: https://www.gnu.org/licenses/gpl-3.0.fr.html


Licence
-------

The project is licensed under the GPL v3.0 license. 